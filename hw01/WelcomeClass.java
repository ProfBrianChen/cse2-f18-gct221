/// Giavanna Tabbachino
/// HW01
/// 9/4/18 
/// Program is supposed to print out "Welcome" and then my Lehigh UserID. 
public class WelcomeClass{
  
  public static void main(String args[]){
    
    System.out.println("   -----------");
    System.out.println("   | WELCOME |");
    System.out.println("   -----------");
    System.out.println("  ^  ^  ^  ^  ^  ^") ;
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");
    System.out.println("<-G--C--T--2--2--1->");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /"); 
    System.out.println("  v  v  v  v  v  v");                   
  } 
} 