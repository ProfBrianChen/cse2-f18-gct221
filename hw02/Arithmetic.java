/// Giavanna Tabbachino
/// 9/11/18 
/// CSE 002 - 210 
/// This program simulates you going clothing shopping and computes the cost of the pants, shirts, and belts you bought, including the 6% Pennsylvania sales tax.  

public class Arithmetic{
  
  public static void main(String args[]){
/// Number of pairs of pants
    int numPants = 3;
/// Cost per pair of pants
    double pantsPrice = 34.98;

/// Number of belts
    int numBelts = 1;
/// Cost per belt
    double beltPrice = 33.99; 
 
/// Number of sweatshirts
    int numShirts = 2; 
/// Cost per shirt
    double shirtPrice = 24.99; 
   
/// Set the tax rate. 
    double paSalesTax = 0.06; 
    
/// Assigning variables for costs of each item WITHOUT TAX. 
    double costPants; /// cost of pants 
    double costBelts; /// cost of belts
    double costShirts; /// cost of sweatshirts 
    double totalCostNoTax; /// total cost of all items without tax 
    
/// Assigning variables for total costs WITH TAX. 
    double totalCostPants; /// total cost of pants
    double totalCostBelts; /// total cost of belts 
    double totalCostShirts; /// total cost of sweatshirts 
    double totalCostWithTax; /// total cost of all items WITH TAX
    
/// Assigning variables for total sales tax on each item. 
    double taxPants; /// tax on pants
    double taxBelts; /// tax on belts
    double taxShirts; /// tax on sweatshirts
    double totalTax; /// total tax on all items 
 
/// Calculating the cost of each item WITHOUT TAX.
    costPants = numPants * pantsPrice; /// total cost of pants without tax 
    costShirts = numShirts * shirtPrice; /// total cost of sweatshirts without tax 
    costBelts = numBelts * beltPrice; /// total cost of belts without tax 
    
/// Calculating the sales tax on each item. 
     taxPants = (numPants * pantsPrice) * paSalesTax; 
     /// Making sure there are only 2 decimal places since the value is money. 
     double taxPants1 = taxPants * 100; 
     int taxPants2 = (int)taxPants1;
     taxPants = taxPants2 / 100.0;
    
     /// Making sure there are only 2 decimal places since the value is money. 
     taxBelts = (numBelts * beltPrice) * paSalesTax; 
         double taxBelts1 = taxBelts * 100; 
     int taxBelts2 = (int)taxBelts1;
     taxBelts = taxBelts2 / 100.0;
    
     /// Making sure there are only 2 decimal places since the value is money. 
     taxShirts = (numShirts * shirtPrice) * paSalesTax; 
     double taxShirts1 = taxShirts * 100; 
     int taxShirts2 = (int)taxShirts1;
     taxShirts = taxShirts2 / 100.0;
    
     totalTax = taxPants + taxShirts + taxBelts; /// Calculating total tax on all items. 
    
/// Calculating the total cost of each item WITH TAX. 
    totalCostPants = costPants + taxPants; /// total cost of pants with tax
    totalCostBelts = costBelts + taxBelts; /// total cost of belts with tax
    totalCostShirts = costShirts + taxShirts; /// total cost of sweatshirts without tax
 
/// Calculating the total cost of purchase before tax.  
    totalCostNoTax = costShirts + costPants + costBelts; 
    
/// Calculating the total paid for this transaction WITH TAX. 
    totalCostWithTax = totalCostPants + totalCostBelts + totalCostShirts; 
    /// Making sure there are only 2 decimal places since the value is money.
    double  totalCostWithTax1 =  totalCostWithTax * 100; 
    int  totalCostWithTax2 = (int) totalCostWithTax1;
    totalCostWithTax =  totalCostWithTax2 / 100.0;
    
    
/// Print total costs of each item and the total sales tax paid for each. 
    System.out.println("The total cost of the pants was $" + costPants + ", and the total sales tax paid on the pants was $" + taxPants + ".");
    System.out.println("The total cost of the belts was $" + costBelts + ", and the total sales tax paid on the belts was $" + taxBelts + ".");
    System.out.println("The total cost of the shirts was $" + costShirts + ", and the total sales tax paid on the shirts was $" + taxShirts + ".");
/// Print total cost of purchases before tax. 
    System.out.println("The total cost of purchases before tax was $" + totalCostNoTax + ".");
/// Print out the total sales tax. 
    System.out.println("The total sales tax was $" + totalTax + ".");     
/// Print the total cost of the purchases including sales tax. 
 System.out.println("The total cost of the purchases with tax was $" + totalCostWithTax + ".");                      
                       
                       
    
    
  }
} 