// Giavanna Tabbachino
// 11/6/18 
// CSE 002 - 210 
// HW 7
// This program is designed give statistics about a string given by user input. 
//

import java.util.Scanner; // Setting up scanner to collect input

public class wordTool{
  
  public static void main (String[] args) {
    Scanner myScanner = new Scanner(System.in); // Declaring scanner 
    String text = sampleText(myScanner); // Getting user input for the text 
    char option; // Initializing option variable for if statement 
    String option1; // Initializing option variable 
    int numWhiteSpaces = 0; // Declaring 
    option = printMenu(myScanner, text); // Printing menu before choosing option 
    // If statement using input choice to use each method 
    if (option == 'c') { // Option to see how many non-whitespace characters there are 
    getNumNonWSCharacters(text, option, numWhiteSpaces);
    }
    if (option == 'w') { // Option to see how many words there are in the text 
    getNumOfWords(text, option, numWhiteSpaces);
    }
    else if (option == 'f') { // Option to see how many times a word is in the text 
    findText(text, myScanner);
    }
    else if (option == 'r') { // Option to replace all exclamation points with periods
      replaceExclamation(text);
    }
    else if (option == 's') {
      shortenSpace(text); // Option to shorten double spaces to single spaces 
    }
    else if (option == 'q') {
    System.exit(0); // Option to quit program
    }
  }
  
  // Method to show user the text they inputted 
  public static String sampleText(Scanner myScanner) {
    System.out.print("Enter a string: "); 
    String text = myScanner.nextLine(); 
    System.out.println("You entered: " + text); 
    return text;
  }
  
  // Method to display the menu 
  public static char printMenu(Scanner myScanner, String text) {
  System.out.println(""); 
  System.out.println("MENU");
  System.out.println("c - Number of non-whitespace characters");
  System.out.println("w - Numbers of words");
  System.out.println("f - Find text");
  System.out.println("r - Replace all !'s");
  System.out.println("s - Shorten spaces");
  System.out.println("q - Quit");
  System.out.println("Choose an option:");
  String option1 = myScanner.next();
  char option = option1.charAt(0);
  return option; 
  }

  // Method to see how many non-whitespace characters there are in the text
  public static void getNumNonWSCharacters(String text, char option, int numWhiteSpaces) {
  int length = text.length();
  System.out.println(length); 
  for (int i = 0; i < length; i++) {
    char position = text.charAt(i); 
    if (Character.isWhitespace(text.charAt(i))) {
      numWhiteSpaces++;
    }
  }
    int realLength = length - numWhiteSpaces;
    System.out.println("Number of non-white space characters: " + realLength); 
  }
 
  // Method to see how many words there are in the text
  public static void getNumOfWords(String text, char option, int numWhiteSpaces) { 
  int length = text.length(); 
  for (int i = 0; i < length; i++) {
    char position = text.charAt(i); 
    if (Character.isWhitespace(text.charAt(i))) {
      numWhiteSpaces++;
    }
  }
  numWhiteSpaces++;
  System.out.println("Number of words: " + numWhiteSpaces); 
  }
 
  // Method to see how many times a word or phrase is in the text
  public static void findText(String text, Scanner myScanner) {
  System.out.println("Enter a word or phrase to be found: ");
  String find = myScanner.next();
  int count = 0;
  int x = find.length();
  int search = 0;
  while (search != -1) {
    search = text.indexOf(find, search);
    if(search != -1) {
    count++;
    search = search + x;
    } 
  }
 System.out.print("\"" + find + "\"" + " instances: " + count);
 }
  
  // Method to replace all exclamation points with periods
  public static void replaceExclamation(String text) {
  String replacement = text.replace("!",".");
  System.out.println("Edited text: " + replacement); 
  }
  
  // Method to shorten all double spaces to single spaces 
  public static void shortenSpace(String text) {
  String shorten = text.replace("  "," ");
  System.out.println("Edited text: " + shorten); 
  }
  
  } 
  
