// Giavanna Tabbachino
// 12/4/18 
// CSE 002 - 210 
// HW10
// This program is designed to simulate a game of tic-tac-toe. 

import java.util.Scanner; 

public class HW10{ 
  public static void main (String[] args) {
  int choice; // Initializing position of mark 
  Scanner myScanner = new Scanner(System.in); // Setting up the scanner to take the input of position 
  String [][] board = new String[3][3]; // Setting up the multidimensional array 
  
  // Setting numbers in the array representing the positions of the tic-tac-toe board
  board[0][0] = "1";
  board[0][1] = "2";
  board[0][2] = "3";
  board[1][0] = "4";
  board[1][1] = "5";
  board[1][2] = "6";
  board[2][0] = "7";
  board[2][1] = "8";
  board[2][2] = "9"; 

  // Printing out the original array with all of the positions 
  System.out.println(board[0][0] + "    " + board[0][1] + "    " + board[0][2]);
  System.out.println(board[1][0] + "    " + board[1][1] + "    " + board[1][2]);
  System.out.println(board[2][0] + "    " + board[2][1] + "    " + board[2][2]);
  System.out.println("Player X will go first. Alternate Player X and Player O when chosing places of the marks.");
  
  for(int i = 1; i < 10; i++) {
  System.out.println("Where would you like to place your mark?"); // Ask for input of the position of the mark 
  choice = myScanner.nextInt(); // Take input of position of mark 
  
  if (i % 2 == 1) { 
   MarksX(choice, board, myScanner); // When Player X is choosing  
  }
  else {
   MarksO(choice, board, myScanner); // When Player O is choosing
  }
  
  // Printing out the new board 
  System.out.println(board[0][0] + "    " + board[0][1] + "    " + board[0][2]);
  System.out.println(board[1][0] + "    " + board[1][1] + "    " + board[1][2]);
  System.out.println(board[2][0] + "    " + board[2][1] + "    " + board[2][2]);
  
  // Check to see if either player has won the game. 
  if((board[0][0] == "X") && (board[0][1] == "X") && (board[0][2] == "X")) {
  System.out.println("The game is over. Player X has won.");
  break;
   }
  else if((board[0][0] == "X") && (board[1][0] == "X") && (board[2][0] == "X")) {
  System.out.println("The game is over. Player X has won.");
  break;
    }
  else if((board[0][1] == "X") && (board[1][1] == "X") && (board[2][1] == "X")){
  System.out.println("The game is over. Player X has won."); 
  break;
    }
  else if((board[0][2] == "X") && (board[1][2] == "X") && (board[2][2] == "X")){
  System.out.println("The game is over. Player X has won."); 
  break;
    }
  else if((board[0][0] == "X") && (board[1][1] == "X") && (board[2][2] == "X")){
  System.out.println("The game is over. Player X has won."); 
  break;
    }
  else if((board[0][2] == "X") && (board[1][1] == "X") && (board[2][0] == "X")){
  System.out.println("The game is over. Player X has won."); 
  break;
    } 
  else if((board[0][0] == "O") && (board[0][1] == "O") && (board[0][2] == "O")) {
  System.out.println("The game is over. Player O has won."); 
  break;
    }
  else if((board[0][0] == "O") && (board[1][0] == "O") && (board[2][0] == "O")) {
  System.out.println("The game is over. Player O has won."); 
  break;
    }
  else if((board[0][1] == "O") && (board[1][1] == "O") && (board[2][1] == "O")){
  System.out.println("The game is over. Player O has won.");
  break;
    }
  else if((board[0][2] == "O") && (board[1][2] == "O") && (board[2][2] == "O")){
  System.out.println("The game is over. Player O has won."); 
  break;
    }
  else if((board[0][0] == "O") && (board[1][1] == "O") && (board[2][2] == "O")){
  System.out.println("The game is over. Player O has won.");
  break;
   }
  else if((board[0][2] == "O") && (board[1][1] == "O") && (board[2][0] == "O")){
  System.out.println("The game is over. Player O has won."); 
  break;
    }
   }
  }

// Method to insert the position of the mark X
public static void MarksX(int choice, String[][] board, Scanner myScanner) {
  switch (choice) {
  case 1:
    if((board[0][0] == "X")) {
    System.out.println("X is already in this spot. Please pick another spot."); // Tell user that there is already an X in that position 
    choice = myScanner.nextInt(); // Take a new input
    MarksX(choice, board, myScanner);} // Use new input to set the position of the mark X
    else if(board[0][0] == "O") { 
    System.out.println("O is already in this spot. Please pick another spot."); // Tell user that there is already an O in that position 
    choice = myScanner.nextInt(); // Take a new input
    MarksX(choice, board, myScanner);} // Use new input to set the position of the mark X
    else {
      board[0][0] = "X"; }
    break;
  case 2:
     if((board[0][1] == "X")) {
    System.out.println("X is already in this spot. Please pick another spot."); 
  choice = myScanner.nextInt();
  MarksX(choice, board, myScanner);}
    else if(board[0][1] == "O") {
    System.out.println("O is already in this spot. Please pick another spot.");
    choice = myScanner.nextInt();
    MarksX(choice, board, myScanner);}
    else {
      board[0][1] = "X"; }
    break;
  case 3: 
    if((board[0][2] == "X")) {
    System.out.println("X is already in this spot. Please pick another spot.");
  choice = myScanner.nextInt();
  MarksX(choice, board, myScanner);}
    else if(board[0][2] == "O") {
    System.out.println("O is already in this spot. Please pick another spot.");
    choice = myScanner.nextInt();
    MarksX(choice, board, myScanner);}
    else {
      board[0][2] = "X"; }
    break;
  case 4:
     if((board[1][0] == "X")) {
    System.out.println("X is already in this spot. Please pick another spot.");
  choice = myScanner.nextInt();
  MarksX(choice, board, myScanner);}
    else if(board[1][0] == "O") {
    System.out.println("O is already in this spot. Please pick another spot."); 
    choice = myScanner.nextInt();
    MarksX(choice, board, myScanner);}
    else {
      board[1][0] = "X"; }
    break; 
  case 5:
     if((board[1][1] == "X")) {
    System.out.println("X is already in this spot. Please pick another spot.");
  choice = myScanner.nextInt();
  MarksX(choice, board, myScanner);}
    else if(board[1][1] == "O") {
    System.out.println("O is already in this spot. Please pick another spot.");
    choice = myScanner.nextInt();
    MarksX(choice, board, myScanner);}
    else {
      board[1][1] = "X"; }
    break;
  case 6:
     if((board[1][2] == "X")) {
    System.out.println("X is already in this spot. Please pick another spot.");
  choice = myScanner.nextInt();
  MarksX(choice, board, myScanner);}
    else if(board[1][2] == "O") {
    System.out.println("O is already in this spot. Please pick another spot."); 
    choice = myScanner.nextInt();
    MarksX(choice, board, myScanner);}
    else {
      board[1][2] = "X"; }
    break;
  case 7:
     if((board[2][0] == "X")) {
    System.out.println("X is already in this spot. Please pick another spot.");
  choice = myScanner.nextInt();
  MarksX(choice, board, myScanner);}
    else if(board[2][0] == "O") {
    System.out.println("O is already in this spot. Please pick another spot.");
    choice = myScanner.nextInt();
    MarksX(choice, board, myScanner);}
    else {
      board[2][0] = "X"; }
    break; 
  case 8:
     if((board[2][1] == "X")) {
    System.out.println("X is already in this spot. Please pick another spot.");
    choice = myScanner.nextInt();
   MarksX(choice, board, myScanner);}
    else if(board[2][1] == "O") {
    System.out.println("O is already in this spot. Please pick another spot.");
    choice = myScanner.nextInt();
    MarksX(choice, board, myScanner);}
    else {
      board[2][1] = "X"; }
    break;
  case 9:
    if((board[2][2] == "X")) {
    System.out.println("X is already in this spot. Please pick another spot.");
  choice = myScanner.nextInt();
  MarksX(choice, board, myScanner);}
    else if(board[2][2] == "O") {
    System.out.println("O is already in this spot. Please pick another spot.");
    choice = myScanner.nextInt();
    MarksX(choice, board, myScanner);}
    else {
      board[2][2] = "X"; }
    break; 
    }
}

// Method to insert the position of the mark O
public static void MarksO(int choice, String[][] board, Scanner myScanner) {
switch (choice) {
  case 1:
    if((board[0][0] == "X")) {
    System.out.println("X is already in this spot. Please pick another spot."); // Tell user that there is already an X in that position 
    choice = myScanner.nextInt(); // Take a new input
    MarksO(choice, board, myScanner);} // Use new input to set the position of the mark O
    else if(board[0][0] == "O") {
    System.out.println("O is already in this spot. Please pick another spot."); // Tell user that there is already an O in that position 
    choice = myScanner.nextInt(); // Take a new input
    MarksO(choice, board, myScanner);} // Use new input to set the position of the mark O
    else {
      board[0][0] = "O"; }
    break; 
    
  case 2:
    if((board[0][1] == "X")) {
    System.out.println("X is already in this spot. Please pick another spot.");
  choice = myScanner.nextInt();
  MarksO(choice, board, myScanner);}
    else if(board[0][1] == "O") {
    System.out.println("O is already in this spot. Please pick another spot."); 
    choice = myScanner.nextInt();
    MarksO(choice, board, myScanner);}
    else {
      board[0][1] = "O"; }
    break;  
  case 3: 
    if((board[0][2] == "X")) {
    System.out.println("X is already in this spot. Please pick another spot.");
  choice = myScanner.nextInt();
  MarksO(choice, board, myScanner);}
    else if(board[0][2] == "O") {
    System.out.println("O is already in this spot. Please pick another spot.");
    choice = myScanner.nextInt();
    MarksO(choice, board, myScanner);}
    else {
      board[0][2] = "O"; }
    break;
  case 4:
    if((board[1][0] == "X")) {
    System.out.println("X is already in this spot. Please pick another spot.");
  choice = myScanner.nextInt();
  MarksO(choice, board, myScanner);}
    else if(board[1][0] == "O") {
    System.out.println("O is already in this spot. Please pick another spot.");
    choice = myScanner.nextInt();
    MarksO(choice, board, myScanner);}
    else {
      board[1][0] = "O"; }
    break; 
  case 5:
    if((board[1][1] == "X")) {
    System.out.println("X is already in this spot. Please pick another spot.");
  choice = myScanner.nextInt();
  MarksO(choice, board, myScanner);}
    else if(board[1][1] == "O") {
    System.out.println("O is already in this spot. Please pick another spot.");
    choice = myScanner.nextInt();
    MarksO(choice, board, myScanner);}
    else {
      board[1][1] = "O"; }
    break;
  case 6:
    if((board[1][2] == "X")) {
    System.out.println("X is already in this spot. Please pick another spot."); 
  choice = myScanner.nextInt();
  MarksO(choice, board, myScanner);}
    else if(board[1][2] == "O") {
    System.out.println("O is already in this spot. Please pick another spot.");
    choice = myScanner.nextInt();
    MarksO(choice, board, myScanner);}
    else {
      board[1][2] = "O"; }
    break;
  case 7:
    if((board[2][0] == "X")) {
    System.out.println("X is already in this spot. Please pick another spot.");
  choice = myScanner.nextInt();
  MarksO(choice, board, myScanner);}
    else if(board[2][0] == "O") {
    System.out.println("O is already in this spot. Please pick another spot.");
    choice = myScanner.nextInt();
      MarksO(choice, board, myScanner);;}
    else {
      board[2][0] = "O"; }
    break; 
  case 8:
    if((board[2][1] == "X")) {
    System.out.println("X is already in this spot. Please pick another spot.");
  choice = myScanner.nextInt();
  MarksO(choice, board, myScanner);}
    else if(board[2][1] == "O") {
    System.out.println("O is already in this spot. Please pick another spot.");
    choice = myScanner.nextInt();
    MarksO(choice, board, myScanner);}
    else {
      board[2][1] = "O"; }
    break;
  case 9:
    if((board[2][2] == "X")) {
    System.out.println("X is already in this spot. Please pick another spot."); 
    choice = myScanner.nextInt();}
    else if(board[2][2] == "O") {
    System.out.println("O is already in this spot. Please pick another spot."); 
    choice = myScanner.nextInt();}
    else {
      board[2][2] = "O"; }
    break;
   }
}

}
   
