///// Giavanna Tabbachino
// 9/19/18 
// CSE 002 - 210 
// Lab 4
// This program is designed to pick a random card from a deck of cards so a magician can practice magic tricks alone. 
//

import java.util.Random; 
public class CardGenerator{
  // main method required for each Java program
  public static void main(String[] args) {
    
   Random randGen = new Random(); // Declaring random generator 
   
   int numberCard = (int)(randGen.nextInt(53)) + 1; // Generate a random number for the card 
   int suitCardNumber = numberCard / 13; // Declaring variable to determine what suit corresponds with the random number
   int suitCardNumber1 = numberCard % 13; // Declaring variable to determine if the card is royal i.e. Ace, Jack, Queen, or King 
  
   String suitCard; // Variable used to print the suit of the card 
   String royalCard; // Variable used to print the royal card if the card is not numbered 2-9 
    
 
   if (suitCardNumber == 0) { // Determine if the card suit is Diamonds 
     suitCard = "Diamonds"; // Used to print suit out 
     switch (suitCardNumber1) { // Checking if the card is royal 
     
        case 1: // If card value is an Ace 
        royalCard = Integer.toString(numberCard); // Transform the value of the card to a royal value 
        royalCard = "Ace"; // Used to print out royal card 
        System.out.println("You picked the " + royalCard + " of " + suitCard); // Print the value and suit of card 
        break; 
    
        case 0: // If card value is a King 
        royalCard = Integer.toString(numberCard); // Transform the value of the card to a royal value 
        royalCard = "King"; // Used to print out royal card 
        System.out.println("You picked the " + royalCard + " of " + suitCard); // Print the value and suit of card 
        break; 
    
        case 12: // If card value is a Queen 
        royalCard = Integer.toString(numberCard); // Transform the value of the card to a royal value 
        royalCard = "Queen"; // Used to print out royal card 
        System.out.println("You picked the " + royalCard + " of " + suitCard); // Print the value and suit of card 
        break; 
    
        case 11: // If card value is a Jack 
        royalCard = Integer.toString(numberCard); // Transform the value of the card to a royal value 
        royalCard = "Jack"; // Used to print out royal card 
        System.out.println("You picked the " + royalCard + " of " + suitCard); // Print the value and suit of card 
        break; 
     
        default: System.out.println("You picked the " + suitCardNumber1 + " of " + suitCard); // Print the value and suit of car
        break; 
 }
     
   }
   else if (suitCardNumber == 1) { // Determine if the card suit is Clubs 
      suitCard = "Clubs"; // Used to print suit 
      switch (suitCardNumber1) { // Checking if the card is royal 
     
        case 1: // If card value is an Ace 
        royalCard = Integer.toString(numberCard); // Transform the value of the card to a royal value 
        royalCard = "Ace"; // Used to print out royal card  
        System.out.println("You picked the " + royalCard + " of " + suitCard); // Print the value and suit of card 
        break; 
    
        case 0:  // If card value is a King 
        royalCard = Integer.toString(numberCard); // Transform the value of the card to a royal value 
        royalCard = "King"; // Used to print out royal card 
        System.out.println("You picked the " + royalCard + " of " + suitCard); // Print the value and suit of card 
        break; 
    
        case 12: // If card value is a Queen 
        royalCard = Integer.toString(numberCard); // Transform the value of the card to a royal value 
        royalCard = "Queen"; // Used to print out royal card 
        System.out.println("You picked the " + royalCard + " of " + suitCard); // Print the value and suit of card 
        break; 
    
        case 11: // If card value is a Jack 
        royalCard = Integer.toString(numberCard); // Transform the value of the card to a royal value 
        royalCard = "Jack"; // Used to print out royal card 
        System.out.println("You picked the " + royalCard + " of " + suitCard); // Print the value and suit of card 
        break; 
     
        default: System.out.println("You picked the " + suitCardNumber1 + " of " + suitCard); // Print the value and suit of car
        break; 
 }
     
   }
   else if (suitCardNumber == 2) { // Determine if the card suit is Hearts 
     suitCard = "Hearts"; // Used to print suit 
     switch (suitCardNumber1) { // Checking if the card is royal 
     
        case 1: // If card value is an Ace 
        royalCard = Integer.toString(numberCard); // Transform the value of the card to a royal value 
        royalCard = "Ace"; // Used to print out royal card 
        System.out.println("You picked the " + royalCard + " of " + suitCard); // Print the value and suit of card 
        break; 
    
        case 0:  // If card value is a King 
        royalCard = Integer.toString(numberCard); // Transform the value of the card to a royal value 
        royalCard = "King"; // Used to print out royal card 
        System.out.println("You picked the " + royalCard + " of " + suitCard); // Print the value and suit of card 
        break; 
    
        case 12: // If card value is a Queen 
        royalCard = Integer.toString(numberCard); // Transform the value of the card to a royal value 
        royalCard = "Queen"; // Used to print out royal card 
        System.out.println("You picked the " + royalCard + " of " + suitCard); // Print the value and suit of card 
        break; 
    
        case 11: // If card value is a Jack 
        royalCard = Integer.toString(numberCard); // Transform the value of the card to a royal value 
        royalCard = "Jack"; // Used to print out royal card 
        System.out.println("You picked the " + royalCard + " of " + suitCard); // Print the value and suit of card 
        break; 
     
        default: System.out.println("You picked the " + suitCardNumber1 + " of " + suitCard); // Print the value and suit of car
        break; 
 }
     
   }
   else if (suitCardNumber == 3) { // Determine if the card suit is Spades 
     suitCard = "Spades"; // Used to print suit 
     switch (suitCardNumber1) { // Checking if the card is royal 
     
        case 1: // If card value is an Ace 
        royalCard = Integer.toString(numberCard); // Transform the value of the card to a royal value 
        royalCard = "Ace"; // Used to print out royal card 
        System.out.println("You picked the " + royalCard + " of " + suitCard); // Print the value and suit of card 
        break; 
    
        case 0:  // If card value is a King 
        royalCard = Integer.toString(numberCard); // Transform the value of the card to a royal value 
        royalCard = "King"; // Used to print out royal card 
        System.out.println("You picked the " + royalCard + " of " + suitCard); // Print the value and suit of card 
        break; 
    
        case 12: // If card value is a Queen 
        royalCard = Integer.toString(numberCard); // Transform the value of the card to a royal value 
        royalCard = "Queen"; // Used to print out royal card 
        System.out.println("You picked the " + royalCard + " of " + suitCard); // Print the value and suit of card 
        break; 
    
        case 11: // If card value is a Jack 
        royalCard = Integer.toString(numberCard); // Transform the value of the card to a royal value 
        royalCard = "Jack"; // Used to print out royal card 
        System.out.println("You picked the " + royalCard + " of " + suitCard); // Print the value and suit of card 
        break; 
     
        default: System.out.println("You picked the " + suitCardNumber1 + " of " + suitCard); // Print the value and suit of card 
        break; 
    }
   }   
  }    
 }   
   