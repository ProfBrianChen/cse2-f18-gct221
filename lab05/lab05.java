//  Giavanna Tabbachino
// 10/3/18 
// CSE 002 - 210 
// Lab 5
// This program is designed to take inputs of information about a college class and return all the information. 
//

import java.util.Scanner;

public class lab05{
  // main method required for each Java program
  public static void main(String[] args) {
   
    // Initialize the valuesjunkWord = myScanner.next(); // Clearing out the information on th
    int courseNum;
    String departName; 
    int courseMeets; 
    int time; 
    String name; 
    int numberStuds; 
    String junk; 
    
    // Set up the scanner
    Scanner myScanner = new Scanner(System.in);
    System.out.print("Enter course number: "); // Asking for input of the course number 
 
  while (!myScanner.hasNextInt()) { // Print out error message if the input is not an integer 
    System.out.println("Error: ");
    junk = myScanner.nextLine(); // Clearing out the information on the input line 
  }
  courseNum = myScanner.nextInt(); // Assign input to the course number 
  junk = myScanner.nextLine(); // Clearing out the information on the input line 
    
  System.out.print("Enter the department name: ");// Asking for input of the department name 
  departName = myScanner.nextLine(); // Assign input to the name of the apartment 
    
  System.out.print("Enter how many times the course meets per week: "); // Asking for input of how many times the course meets per week 
  while (!myScanner.hasNextInt()) { // Print out error message if the input is not an integer 
    System.out.println("Error: ");
    junk = myScanner.nextLine(); // Clearing out the unwanted information on the input line 
  }
  courseMeets = myScanner.nextInt(); // Assign input to how many times the course meets per week 
  junk = myScanner.nextLine(); // Clearing out the unwanted information on the input line 
  
  System.out.print("Enter the time the course meets: "); // Asking for input of the time the course meets  
  while (!myScanner.hasNextInt()) { // Print out error message if the input is not an integer 
    System.out.println("Error: ");
    junk = myScanner.nextLine(); // Clearing out the unwanted information on the input line 
  }
  time = myScanner.nextInt(); // Assign input to the time the class starts 
  junk = myScanner.nextLine(); // Clearing out the unwanted information on the input line 
  
  System.out.print("Enter name of the professor: "); // Asking for input of the name of the professor who teaches the class 
  name = myScanner.nextLine(); // Assign input to the name of the professor who teaches the class 
   
  System.out.print("Enter the number of students: "); // Asking for input of the number of students in the class 
  while (!myScanner.hasNextInt()) { // Print out error message if the input is not an integer 
    System.out.println("Error: ");
    junk = myScanner.nextLine(); // Clearing out the unwanted information on the input line 
  }
  numberStuds = myScanner.nextInt(); // Assign input to the number of students in the class 
  junk = myScanner.nextLine(); // Clearing out the unwanted information on the input line 
    
 
 // Print out the inputs  
 System.out.println("The course number is " + courseNum + ".");
 System.out.println("The department name is " + departName + ".");
 System.out.println("The course meets " + courseMeets + " times a week.");
 System.out.println("The course meets at " +  time + ".");
 System.out.println("The professor's name is " + name + ".");
 System.out.println("There are " + numberStuds + " students in the class.");
   
 
  } 
} 

 
 