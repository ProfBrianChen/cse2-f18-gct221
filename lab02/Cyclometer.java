/// Giavanna Tabbachino
/// 9/5/18 
// CSE 002 - 210 
/// This program is designed to model a bicyle Cyclometer and measure/print the number of minutes for each trip, number of counts for each trip, distance of each trip, and distance for two trips combined. 
//
public class Cyclometer {
    // main method required for each Java program
  public static void main(String[] args) {
    int secsTrip1 = 480; // Number of seconds for Trip 1
    int secsTrip2 = 3220; // Number of seconds for Trip 2
    int countsTrip1 = 1561; // Number of rotations of the bicycle wheel for Trip 1
    int countsTrip2 = 9037; // Number of rotations of the bicycle wheel for Trip 2
    
    double wheelDiameter = 27.0; // Diameter of the bicycle wheel
    double PI = 3.14159; // Value for pi 
    int feetPerMile = 5280; // Number of feet in one mile
    int inchesPerFoot = 12; // Number of inches in one foot
    int secondsPerMinute = 60; // Number of seconds in one minutes
    double distanceTrip1; // Distance of Trip 1 in miles
    double distanceTrip2; // Distance of Trip 2 in miles 
    double totalDistance; // Total distance of Trip 1 and Trip 2 
    
    // Run the calculations for the time each trip took and how many rotations there were on the bicycle for each trip. 
    // Store these values. Time is in units of minutes. Rotations are in units of counts.  
    System.out.println("Trip 1 took " + ((double)secsTrip1 / secondsPerMinute) + " minutes and had " + countsTrip1 + " counts.");
    System.out.println("Trip 2 took " + ((double)secsTrip2 / secondsPerMinute) + " minutes and had " + countsTrip2 + " counts.");
    
    distanceTrip1 = countsTrip1 * wheelDiameter * PI; // Distance given in units of inches. 
    // For each count, a rotation of the wheel travels the diameter in inches multiplied by pi. 
    
    distanceTrip1 /= inchesPerFoot * feetPerMile; // Distance calculated by given in miles.
    distanceTrip2 = countsTrip2 * wheelDiameter * PI / inchesPerFoot / feetPerMile; // Distance calculated by using the rotations of the wheels.  
    totalDistance = distanceTrip1 + distanceTrip2; // Calculates the total distance of both trips in miles. 
    
    // Print out the output data of the Cyclometer. 
    System.out.println("Trip 1 was " + distanceTrip1 + " miles");
    System.out.println("Trip 2 was " + distanceTrip2 + " miles");
    System.out.println("The total distance was " + totalDistance + " miles");
    
  
  } // end of main method 
} // end of class 