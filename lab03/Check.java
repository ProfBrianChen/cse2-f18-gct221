///// Giavanna Tabbachino
/// 9/12/18 
// CSE 002 - 210 
/// Lab 3
/// This program is designed to model a group of friends going out to dinner and splitting the check evenly. 
///

import java.util.Scanner;

public class Check{
   // main method required for each Java program
  public static void main(String[] args) {
    Scanner myScanner = new Scanner(System.in); // Declare the object of the scanner to take the input values 
    System.out.print("Enter the original cost of the check in the form xx.xx: "); // Asking for user input for the price of the bill 
    double checkCost = myScanner.nextDouble(); // Creating the variable of the cost of the check from the user input; 
    
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): "); // Asking for user input for the tip percentage that they want to pay 
    double tipPercent = myScanner.nextDouble(); 
    tipPercent /= 100; // To convert the percentage into a decimal value 
    
    System.out.print("Enter the number of people who went out to dinner: "); // Asking for user input of the number of people who went to dinner/how many ways the check should be split
    int numPeople = myScanner.nextInt(); // Creating the variable of the number of people who went out to dinner
    
    double totalCost;
    double costPerPerson;
    int dollars, dimes, pennies; // Dollar amount of cost$, cents amount of cost$
    totalCost = checkCost * (1 + tipPercent); /// Calculating total cost
    costPerPerson = totalCost / numPeople; /// Calculating cost per person 
    dollars = (int)costPerPerson; /// Getting the whole amount and dropping the decimal fraction by making the double value into an integer value 
    dimes = (int)(costPerPerson * 10) % 10; /// Getting the dimes amount and making it an integer
    pennies = (int)(costPerPerson * 100) % 10; /// Getting the pennies amount and making it an integer
    
    System.out.println("Each person in the group owes $" + dollars + "." + dimes + pennies); /// Print out the price each person has to pay. 
      
    
  } // end of main method 
} // end of class 