// Giavanna Tabbachino
/// 9/18/18 
// CSE 002 - 210 
/// This program is designed to calculate the number of acres affected by hurrican precipation and how many inches of rain were dropped on average. 
//
import java.util.Scanner;

public class Convert{
   // main method required for each Java program
  public static void main(String[] args) {
    Scanner myScanner = new Scanner(System.in); // Declaring the object of the scanner to take the input values 
    System.out.print("Enter the affected area in acres: "); // Asking for user input for the affected area in acres 
    double acres = myScanner.nextDouble(); // Creating the variable of the affected area in acres from the user input 
    System.out.print("Enter the rainfall in the affected area in inches: "); // Asking for user input for the rainfall in the affected area in inches
    double rainfall = myScanner.nextDouble(); // Creating the variable of the rainfull from the user input; 
    
    double milesCubed; // Declaring variable of the cubic miles affected by hurricane precipation
     
    double sqMilePerAcre = 640.0; // Declaring variable of how many square miles there are per acre 
    double inchesPerFoot = 12.0; // Declaring how many inches there are per foot
    double feetPerMile = 5280.0; // Declaring how many feet there are per mile 
 
    double sqMiles = acres / sqMilePerAcre; // Calculating how many square miles there are in the input number of acres
    double rainfallMiles = rainfall / inchesPerFoot / feetPerMile; // Calculating the value of rainful in units of miles
    double cubicMiles = sqMiles * rainfallMiles; // Calculating the amount of rainfall in cubic miles 
   
    System.out.println(cubicMiles + " cubic miles"); // Printing out the restul in cubic miles 
  } 
} 