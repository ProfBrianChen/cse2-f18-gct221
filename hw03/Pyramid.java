// Giavanna Tabbachino
/// 9/18/18 
// CSE 002 - 210 
/// This program is designed to calculate the volume inside a pyramid when given the dimensions of the pyramid. 
//
import java.util.Scanner;

public class Pyramid{
   // main method required for each Java program
  public static void main(String[] args) {
    Scanner myScanner = new Scanner(System.in); // Declare the object of the scanner to take the input values 
    
    System.out.print("Enter length of the square side of the pyramid: "); // Asking for user input for the length of the square side of the pyramid 
    double squareLength = myScanner.nextDouble(); // Creating the variable of the length of the square side of the pyramid
    
    System.out.print("Enter the height of the pyramid: "); // Asking for user input for the height of the pyramid 
    double height = myScanner.nextDouble(); // Creating the variable of the height of the pyramid  

    double volume = squareLength * squareLength * height / 3; // Calculating the volume of the pyramid
    
    System.out.println("The volume inside the pyramid is: " + volume + "."); // Print out the volume result. 
  } 
} 