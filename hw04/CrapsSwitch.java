/// Giavanna Tabbachino 
/// 9/25/18 
// CSE 002 - 210 
/// This program is designed to simulate rolling dice in a game of Craps and determining the slang value of the outcome using switch statements. 
// 

import java.util.Scanner;
import java.util.Random; 

public class CrapsSwitch{
   // main method required for each Java program
  public static void main(String[] args) {
    
Scanner myScanner = new Scanner(System.in); // Declaring the object of the scanner
System.out.println("Would you like to randomly cast two dice values or input two dice values to evaluate?"); // Asking if user wants random dice values or to input dice values
System.out.print("Type 'random' for two random numbers or 'input' to input two numbers: "); 
String choice = myScanner.nextLine(); // Declaring variable to establish if user wants random or to state dice values 
    
switch (choice) {
  case "random": // If user chooses for the program to randomly cast two dice values 
    Random randGen = new Random(); // Declaring random generator 
      int dice1 = (int)(randGen.nextInt(6) + 1); // Generate first random dice value 
      int dice2 = (int)(randGen.nextInt(6) + 1); // Generate second random dice value
      System.out.println("You rolled " + dice1 + " and " + dice2 + "."); // Print what values were generated  
    switch (dice1) { // Determine the value of the first die 
        case 1: // When the value of the first die is 1  
         switch (dice2) { // Determine the slang terminology of the dice roll using the second die value and printing it  
           case 1: 
             System.out.println("You rolled Snake Eyes.");
             System.exit(0); // Exit 
             break;
           case 2: 
             System.out.println("You rolled an Ace Deuce."); 
             System.exit(0); // Exit
             break;
           case 3: 
             System.out.println("You rolled an Easy Four.");
             System.exit(0); // Exit
             break;
           case 4: 
              System.out.println("You rolled a Fever Five.");
             System.exit(0); // Exit
             break;
           case 5:
              System.out.println("You rolled an Easy Six.");
             System.exit(0); // Exit
             break;
           case 6: 
              System.out.println("You rolled Seven out.");
             System.exit(0); // Exit
             break;
             }
        break;
        case 2: // When the value of the first die is 2
          switch (dice2) { // Determine the slang terminology of the dice roll using the second die value and printing it 
           case 1: 
             System.out.println("You rolled Ace Deuce.");
              System.exit(0); // Exit
              break;
           case 2: 
             System.out.println("You rolled a Hard Four.");
              System.exit(0); // Exit
              break;
           case 3: 
             System.out.println("You rolled a Fever Five.");
              System.exit(0); // Exit
              break;
           case 4: 
              System.out.println("You rolled an Easy Six.");
              System.exit(0); // Exit
              break;
           case 5:
              System.out.println("You rolled Seven out.");
              System.exit(0); // Exit
              break;
           case 6: 
              System.out.println("You rolled an Easy Eight.");
              System.exit(0); // Exit
              break;
             }
        break;
        case 3: // When the value of the first die is 3
           switch (dice2) { // Determine the slang terminology of the dice roll using the second die value and printing it 
           case 1: 
             System.out.println("You rolled an Easy Four.");
               System.exit(0); // Exit
               break;
           case 2: 
             System.out.println("You rolled a Fever Five.");
               System.exit(0); // Exit
               break;
           case 3: 
             System.out.println("You rolled a Hard Six.");
               System.exit(0); // Exit
               break;
           case 4: 
              System.out.println("You rolled Seven out.");
               System.exit(0); // Exit
               break;
           case 5:
              System.out.println("You rolled an Easy Eight.");
               System.exit(0); // Exit
               break;
           case 6: 
              System.out.println("You rolled a Nine.");
               System.exit(0); // Exit
               break;
             }
        break;
        case 4: // When the value of the first die is 4
           switch (dice2) { // Determine the slang terminology of the dice roll using the second die value and printing it 
           case 1: 
             System.out.println("You rolled a Fever Five.");
               System.exit(0); // Exit
               break;
           case 2: 
             System.out.println("You rolled an Easy Six.");
               System.exit(0); // Exit
               break;
           case 3: 
             System.out.println("You rolled Seven out.");
               System.exit(0); // Exit
               break;
           case 4: 
              System.out.println("You rolled Hard Eight.");
               System.exit(0); // Exit
               break;
           case 5:
              System.out.println("You rolled a Nine.");
               System.exit(0); // Exit
               break;
           case 6: 
              System.out.println("You rolled an Easy Ten.");
               System.exit(0); // Exit
               break;
             }
        break;
        case 5: // When the value of the first die is 5
          switch (dice2) { // Determine the slang terminology of the dice roll using the second die value and printing it 
           case 1: 
             System.out.println("You rolled an Easy Six.");
              System.exit(0); // Exit
              break;
           case 2: 
             System.out.println("You rolled Seven out.");
              System.exit(0); // Exit
              break;
           case 3: 
             System.out.println("You rolled an Easy Eight.");
              System.exit(0); // Exit
              break;
           case 4: 
              System.out.println("You rolled a Nine.");
              System.exit(0); // Exit
              break;
           case 5:
              System.out.println("You rolled a Hard Ten.");
              System.exit(0); // Exit
              break;
           case 6: 
              System.out.println("You rolled a Yo-leven.");
              System.exit(0); // Exit
              break;
             }
        break;
        case 6: // When the value of dice1 is 6 
          switch (dice2) { // Determine the slang terminology of the dice roll using the second die value and printing it 
           case 1: 
             System.out.println("You rolled a Seven out.");
              System.exit(0); // Exit
              break;
           case 2: 
             System.out.println("You rolled an Easy Eight.");
              System.exit(0); // Exit
              break;
           case 3: 
             System.out.println("You rolled a Nine.");
              System.exit(0); // Exit
              break;
           case 4: 
              System.out.println("You rolled an Easy Ten.");
              System.exit(0); // Exit
              break; 
           case 5:
              System.out.println("You rolled a Yo-leven.");
              System.exit(0); // Exit
              break;
           case 6: 
              System.out.println("You rolled a Boxcars.");
              System.exit(0); // Exit
              break;
             }
        break;
      }
    
    
  
  case "input": // If user chooses to input the values of the dice for the program 
    System.out.print("Enter the first dice number using an integer from 1 to 6: "); // Asks user for first dice value 
      int dice3 = myScanner.nextInt(); // Retrieving first dice value from user input 
      System.out.print("Enter the second dice number using an integer from 1 to 6: "); // Asks user for second dice value 
      int dice4 = myScanner.nextInt(); // Retrieving second dice value from user input 
      // if (((dice3) <= 6) && ((dice3) >= 1))  { // Checking that the user input values are in the range 1-6 inclusive 
      System.out.println("You rolled " + dice3 + " and " + dice4 + "."); // Print what values user chose 
       
      switch (dice3) {  // Determine the value of the first die 
        case 1: // When the value of the first die is 1 
         switch (dice4) { // Determine the slang terminology of the dice roll using the second die value and printing it 
           case 1: 
             System.out.println("You rolled Snake Eyes.");
             System.exit(0); // Exit
             break;
           case 2: 
             System.out.println("You rolled an Ace Deuce.");
             System.exit(0); // Exit
             break;
           case 3: 
             System.out.println("You rolled an Easy Four.");
             System.exit(0); // Exit
             break;
           case 4: 
              System.out.println("You rolled a Fever Five.");
             System.exit(0); // Exit
             break;
           case 5:
              System.out.println("You rolled an Easy Six.");
             System.exit(0); // Exit
             break;
           case 6: 
              System.out.println("You rolled Seven out.");
             System.exit(0); // Exit
             break;
             }
          break;
        case 2: // When the value of the first die is 2 
          switch (dice4) { // Determine the slang terminology of the dice roll using the second die value and printing it 
           case 1: 
             System.out.println("You rolled Ace Deuce.");
              System.exit(0); // Exit
              break;
           case 2: 
             System.out.println("You rolled a Hard Four.");
              System.exit(0); // Exit
              break;
           case 3: 
             System.out.println("You rolled a Fever Five.");
              System.exit(0); // Exit
              break;
           case 4: 
              System.out.println("You rolled an Easy Six.");
              System.exit(0); // Exit
              break;
           case 5:
              System.out.println("You rolled Seven out.");
              System.exit(0); // Exit
              break;
           case 6: 
              System.out.println("You rolled an Easy Eight.");
              System.exit(0); // Exit
              break;
             }
          break;
        case 3: // When the value of the first die is 3
           switch (dice4) { // Determine the slang terminology of the dice roll using the second die value and printing it 
           case 1: 
             System.out.println("You rolled an Easy Four.");
               System.exit(0); // Exit
               break;
           case 2: 
             System.out.println("You rolled a Fever Five.");
               System.exit(0); // Exit
               break;
           case 3: 
             System.out.println("You rolled a Hard Six.");
               System.exit(0); // Exit
               break;
           case 4: 
              System.out.println("You rolled Seven out.");
               System.exit(0); // Exit
               break;
           case 5:
              System.out.println("You rolled an Easy Eight.");
               System.exit(0); // Exit
               break;
           case 6: 
              System.out.println("You rolled a Nine.");
               System.exit(0); // Exit
               break;
             }
          break;
        case 4: // When the value of the first die is 4
           switch (dice4) { // Determine the slang terminology of the dice roll using the second die value and printing it 
           case 1: 
             System.out.println("You rolled a Fever Five.");
               System.exit(0); // Exit
               break;
           case 2: 
             System.out.println("You rolled an Easy Six.");
               System.exit(0); // Exit
               break;
           case 3: 
             System.out.println("You rolled Seven out.");
               System.exit(0); // Exit
               break;
           case 4: 
              System.out.println("You rolled Hard Eight.");
               System.exit(0); // Exit
               break;
           case 5:
              System.out.println("You rolled a Nine.");
               System.exit(0); // Exit
               break;
           case 6: 
              System.out.println("You rolled an Easy Ten.");
               System.exit(0); // Exit
               break;
             }
          break;
        case 5: // When the value of the first die is 5
          switch (dice4) { // Determine the slang terminology of the dice roll using the second die value and printing it 
           case 1: 
             System.out.println("You rolled an Easy Six.");
              System.exit(0); // Exit
              break;
           case 2: 
             System.out.println("You rolled Seven out.");
              System.exit(0); // Exit
              break;
           case 3: 
             System.out.println("You rolled an Easy Eight.");
              System.exit(0); // Exit
              break;
           case 4: 
              System.out.println("You rolled a Nine.");
              System.exit(0); // Exit
              break;
           case 5:
              System.out.println("You rolled a Hard Ten.");
              System.exit(0); // Exit
              break;
           case 6: 
              System.out.println("You rolled a Yo-leven.");
              System.exit(0); // Exit
              break;
             }
          break;
        case 6: // When the value of the first die is 6
          switch (dice4) { // Determine the slang terminology of the dice roll using the second die value and printing it 
           case 1: 
             System.out.println("You rolled an Easy Ten.");
              System.exit(0); // Exit
              break;
           case 2: 
             System.out.println("You rolled an Easy Eight.");
              System.exit(0); // Exit
              break;
           case 3: 
              System.out.println("You rolled a Nine.");
              System.exit(0); // Exit
              break;
           case 4: 
              System.out.println("You rolled an Easy Ten.");
              System.exit(0); // Exit
              break;
           case 5:
              System.out.println("You rolled a Yo-leven.");
              System.exit(0); // Exit
              break;
           case 6: 
              System.out.println("You rolled a Boxcars.");
              System.exit(0); // Exit
              break;
             }
          break;     
      }
    }
}
} 
      
