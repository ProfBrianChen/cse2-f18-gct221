/// Giavanna Tabbachino 
/// 9/25/18 
// CSE 002 - 210 
/// This program is designed to simulate rolling dice in a game of Craps and determining the slang value of the outcome using if and if else statements. 
// 

import java.util.Scanner;
import java.util.Random; 

public class CrapsIf{
   // main method required for each Java program
  public static void main(String[] args) {
    
    Scanner myScanner = new Scanner(System.in); // Declaring the object of the scanner
    System.out.println("Would you like to randomly cast two dice values or input two dice values to evaluate?"); // Asking if user wants random dice values or to input dice values
    System.out.print("Type 'random' for two random numbers or 'input' to input two numbers: "); 
    String choice = myScanner.nextLine(); // Declaring variable to establish if user wants random or to state dice values 
    
    if (choice.equals("random")) { // Simulation begins if user wants random dice values 
      Random randGen = new Random(); // Declaring random generator 
      int dice1 = (int)(randGen.nextInt(6) + 1); // Generate first random dice value 
      int dice2 = (int)(randGen.nextInt(6) + 1); // Generate second random dice value
      System.out.println("You rolled " + dice1 + " and " + dice2 + "."); // Print what values were generated  
      if ((((dice1) == 1) && ((dice2) == 1))) { // If statements printing the slang terminology describing the outcome of the roll of two dice  
        System.out.println("You rolled Snake Eyes.");
        System.exit(0); // Exit
      }
      else if (((((dice1) == 1) && ((dice2) == 2)) || (((dice1) == 2) && ((dice2) == 1)))) {
        System.out.println("You rolled an Ace Deuce.");
        System.exit(0); // Exit
      }
      else if (((dice1) == 2) && ((dice2) == 2)) {
        System.out.println("You rolled a Hard Four.");
        System.exit(0); // Exit
      }
      else if (((((dice1) == 1) && ((dice2) == 3)) || (((dice1) == 3) && ((dice2) == 1)))) {
        System.out.println("You rolled an Easy Four.");
        System.exit(0); // Exit
      }
      else if (((((dice1) == 2) && ((dice2) == 3)) || (((dice1) == 3) && ((dice2) == 2)))) {
        System.out.println("You rolled a Fever Five.");
        System.exit(0); // Exit
      }
     else if (((dice1) == 3) && ((dice2) == 3))  {
        System.out.println("You rolled a Hard Six.");
       System.exit(0); // Exit
      }
      else if (((((dice1) == 2) && ((dice2) == 4)) || (((dice1) == 4) && ((dice2) == 2)))) {
        System.out.println("You rolled an Easy Six.");
        System.exit(0); // Exit
      }
     else if (((((dice1) == 3) && ((dice2) == 4)) || (((dice1) == 4) && ((dice2) == 3)))) {
        System.out.println("You rolled Seven out.");
        System.exit(0); // Exit
      }
     else if (((dice1) == 4) && ((dice2) == 4)) {
        System.out.println("You rolled a Hard Eight.");
        System.exit(0); // Exit
      }
     else if (((((dice1) == 3) && ((dice2) == 5)) || (((dice1) == 5) && ((dice2) == 3)))) {
        System.out.println("You rolled an Easy Eight."); 
        System.exit(0); // Exit
      } 
      else if (((((dice1) == 4) && ((dice2) == 5)) || (((dice1) == 5) && ((dice2) == 4)))) {
        System.out.println("You rolled a Nine.");
        System.exit(0); // Exit
      }
      else if (((((dice1) == 4) && ((dice2) == 6)) || (((dice1) == 6) && ((dice2) == 4)))) {
        System.out.println("You rolled an Easy Ten.");
        System.exit(0); // Exit
      }
      else if (((dice1) == 5) && ((dice2) == 5)) {
        System.out.println("You rolled a Hard Ten.");
        System.exit(0); // Exit
      }
      else if (((((dice1) == 5) && ((dice2) == 6)) || (((dice1) == 6) && ((dice2) == 5)))) {
        System.out.println("You rolled a Yo-leven.");  
        System.exit(0); // Exit
      }
      else if (((dice1) == 6) && ((dice2) == 6)) {
        System.out.println("You rolled Boxcars.");
        System.exit(0); // Exit
      }
    }         
     
     
    if (choice.equals("input")) { // Simulation begins if user inputs two dice values 
      System.out.print("Enter the first dice number using an integer from 1 to 6: "); // Asks user for first dice value 
      int dice3 = myScanner.nextInt(); // Retrieving first dice value from user input 
      System.out.print("Enter the second dice number using an integer from 1 to 6: "); // Asks user for second dice value 
      int dice4 = myScanner.nextInt(); // Retrieving second dice value from user input 
      if (((dice3) <= 6) && ((dice3) >= 1))  { // Checking that the user input values are in the range 1-6 inclusive 
      System.out.println("You rolled " + dice3 + " and " + dice4 + "."); // Print what values user chose 
        if ((((dice3) == 1) && ((dice4) == 1))) { // If statements printing the slang terminology describing the outcome of the roll of two dice  
          System.out.println("You rolled Snake Eyes.");
      }
        else if (((((dice3) == 1) && ((dice4) == 2)) || (((dice3) == 2) && ((dice4) == 1)))) {
          System.out.println("You rolled an Ace Deuce.");
      }
        else if (((dice3) == 2) && ((dice4) == 2)) {
          System.out.println("You rolled a Hard Four.");
      }
        else if (((((dice3) == 1) && ((dice4) == 3)) || (((dice3) == 3) && ((dice4) == 1)))) {
          System.out.println("You rolled an Easy Four.");
      }
        else if (((((dice3) == 2) && ((dice4) == 3)) || (((dice3) == 3) && ((dice4) == 2)))) {
          System.out.println("You rolled a Fever Five.");
      }
        else if (((dice3) == 3) && ((dice4) == 3))  {
           System.out.println("You rolled a Hard Six.");
      }
        else if (((((dice3) == 2) && ((dice4) == 4)) || (((dice3) == 4) && ((dice4) == 2)))) {
           System.out.println("You rolled an Easy Six.");
      }
        else if (((((dice3) == 3) && ((dice4) == 4)) || (((dice3) == 4) && ((dice4) == 3)))) {
           System.out.println("You rolled Seven out.");
      }
        else if (((dice3) == 4) && ((dice4) == 4)) {
            System.out.println("You rolled a Hard Eight.");
      }
        else if (((((dice3) == 3) && ((dice4) == 5)) || (((dice3) == 5) && ((dice4) == 3)))) {
            System.out.println("You rolled an Easy Eight."); 
      }
        else if (((((dice3) == 4) && ((dice4) == 5)) || (((dice3) == 5) && ((dice4) == 4)))) {
            System.out.println("You rolled a Nine.");
      }
        else if (((((dice3) == 4) && ((dice4) == 6)) || (((dice3) == 6) && ((dice4) == 4)))) {
            System.out.println("You rolled an Easy Ten.");
      }
        else if (((dice3) == 5) && ((dice4) == 5)) {
            System.out.println("You rolled a Hard Ten.");
      }
        else if (((((dice3) == 5) && ((dice4) == 6)) || (((dice3) == 6) && ((dice4) == 5)))) {
            System.out.println("You rolled a Yo-leven.");  
      }
        else if (((dice3) == 6) && ((dice4) == 6)) {
            System.out.println("You rolled Boxcars.");
      }
        
      } 
      else {
        System.out.println("One or both of the numbers for the dice are out of range."); // Printing that the values are out of the range and no slang terminology is printed 
      }
    } } }   
        
    
